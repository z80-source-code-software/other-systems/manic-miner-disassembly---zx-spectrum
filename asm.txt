An ASM version of the disassembly in this archive can be built from the source
files in the 'sources' subdirectory using version 8.0 of SkoolKit.

SkoolKit can be obtained from the following location:

  https://skoolkit.ca

The ASM disassembly can be built by following these steps:

1. Download and unpack SkoolKit 8.0.

2. Copy every file from the 'sources' subdirectory in this archive to the
   directory where SkoolKit was unpacked.

3. Change to the directory where SkoolKit was unpacked and run this command:

  $ ./skool2asm.py mm.skool > mm.asm

Now mm.asm can be assembled using pasmo (http://pasmo.speccy.org/):

  $ pasmo mm.asm mm.bin

Alternatively, mm.skool can be converted to a binary file by the skool2bin.py
utility, included with SkoolKit:

  $ ./skool2bin.py -i -S 32765 mm.skool

To create a Z80 snapshot from mm.bin, use the bin2sna.py utility, included
with SkoolKit:

  $ ./bin2sna.py mm.bin

The resultant snapshot, mm.z80, can be loaded into an emulator, and the game
will start automatically.

To create a TAP file from mm.bin, use the bin2tap.py utility, included with
SkoolKit:

  $ ./bin2tap.py mm.bin

The resultant TAP file, mm.tap, can be loaded into an emulator, and the game
will start automatically.
